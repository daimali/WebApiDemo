
using Swashbuckle.Application;
using System.Reflection;
using System.Web;
using System.Web.Http;
using WebApiDemo;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace WebApiDemo
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var _thisAssembly = typeof(SwaggerConfig).Assembly;
            var _project = MethodBase.GetCurrentMethod().DeclaringType.Namespace;//项目命名空间
            var _xmlPath = string.Format("{0}/bin/{1}.XML", System.AppDomain.CurrentDomain.BaseDirectory, _project);
            var _jsPath = string.Format("{0}.Scripts.swaggerui.swagger_lang.js", _project);

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "WebAPI接口设计：SwaggerUI文档 / 统一响应格式 / 统一异常处理 / 统一权限验证 ");//名称可自定义
                        //xml文件路径
                        c.IncludeXmlComments(_xmlPath);
                        //忽略标记为已删除的属性
                        c.IgnoreObsoleteProperties();
                        //自定义行为，对控制器描述进行缓存(此处若不使用，则无法显示控制器上面的注释)
                        c.CustomProvider((defaultProvider) => new CachingSwaggerProvider(defaultProvider, _xmlPath));
                       
                    })
                .EnableSwaggerUi(c =>
                    {
                        //扩展js  路径规则:项目命名空间.文件夹名称.js文件名称
                        c.InjectJavaScript(_thisAssembly, _jsPath);
                        //默认显示列表
                        c.DocExpansion(DocExpansion.List);
                    });
        }
    }
}
