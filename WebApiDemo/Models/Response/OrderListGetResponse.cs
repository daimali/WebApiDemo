﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDemo.Models.Response
{
    public class OrderListGetResponse
    {
        /// <summary>
        /// 订单列表
        /// </summary>
        public List<OrderModel> List { get; set; }
        /// <summary>
        /// 总数量
        /// </summary>
        public int total { get; set; }
    }
    public class OrderModel
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public long OrderId { get; set; }
        /// <summary>
        /// 下单人
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        public DateTime OrderDate { get; set; }
    }
}