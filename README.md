# WebApiDemo

#### 项目介绍
本项目结合实际工作整理了swaggerui，统一响应格式，异常处理，权限验证等常用模块，并提供一套完善的案例源代码，在实际工作中可直接参考使用

预览地址：http://sapi.daimali.com/swagger/ui/index （暂时无法访问，大家本机部署好访问 http://localhost:port/swagger/ui/index）

WebAPI接口设计：SwaggerUI文档 / 统一响应格式 / 统一异常处理 / 统一权限验证

公众号：![输入图片说明](https://images.gitee.com/uploads/images/2018/1118/125934_15a8491f_907611.jpeg "qrcode_for_gh_05412e662437_258.jpg")
